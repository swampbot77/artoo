# Import required libraries
import sys
import time
import RPi.GPIO as GPIO
import webiopi
import random
import pygame
from threading import Thread

rpiver = GPIO.RPI_INFO
gpiver = GPIO.VERSION

print ("RPI Version = ", rpiver)
print ("GPIO version = ", gpiver)

################################################################
# to enable webiopi debug mode
# webiopi.setDebug()

# Using GPIO in Board Pin number
# GPIO.setwarnings(False)
# GPIO.setmode(GPIO.BCM)  # set board mode to Broadcom
GPIO.setmode(GPIO.BOARD)
# mode = GPIO.getmode()

# Define GPIO signals to use
# Physical pins 11,15,16,18,29,31, 32, 36
# GPIO17,GPIO22,GPIO23,GPIO24, GPIO5,GPIO6, GPIO12, GPIO16


ForwardPort=11
ReversePort=15
ForwardStarboard=16
ReverseStarboard=18
DomeRotateLeft=29
DomeRotateRight=31
DomeLEDRedFront = 32  # not wired
DomeLEDYellowBack = 36 # not wired
DomeLEDBlueFront = 7 # not wired
DomeLEDHologram = 8 # not wired
ProjectorToggle = 23 # not wired

######## GPIO Setup

def setPinModes():
    GPIO.setup(ForwardPort, GPIO.OUT)
    GPIO.setup(ReversePort, GPIO.OUT)
    GPIO.setup(ForwardStarboard, GPIO.OUT)
    GPIO.setup(ReverseStarboard, GPIO.OUT)
    GPIO.setup(DomeRotateLeft, GPIO.OUT)
    GPIO.setup(DomeRotateRight, GPIO.OUT)
    GPIO.setup(DomeLEDRedFront, GPIO.OUT)
    GPIO.setup(DomeLEDYellowBack, GPIO.OUT)
    GPIO.setup(DomeLEDBlueFront, GPIO.OUT)
    GPIO.setup(DomeLEDHologram, GPIO.OUT)
    GPIO.setup(ProjectorToggle, GPIO.OUT)

# RuntimeError: The GPIO channel has not been set up as an OUTPUT
# GPIO.output(ForwardPort, False)
# GPIO.output(ReversePort, False)
# GPIO.output(ForwardStarboard, False)
# GPIO.output(ReverseStarboard, False)
# GPIO.output(DomeRotateLeft, False)
# GPIO.output(DomeRotateRight, False)

#################################################################

# Update The Raspberry Pi's IO pins for leg motor movement and dome rotation.
def updatePins():
    # Special case stuff for Dig Dug mode. Plays the Dig Dug music if R2 is moving. Stops it if not.
    if pygame.digDugRunning == True:
        if pygame.bForward == True or pygame.bReverse == True or pygame.bLeft == True or pygame.bRight == True:
            if pygame.digDugPaused == True:
                pygame.digDugPaused = False
                pygame.mixer.unpause()
        else:
            if pygame.digDugPaused == False:
                pygame.digDugPaused = True
                pygame.mixer.pause()
                
    if pygame.bForward == True or pygame.bReverse == True:
        if pygame.bLeft == True:
            if pygame.bReverse == True:
                GPIO.output(ForwardStarboard, False)
                GPIO.output(ReverseStarboard, True)
                GPIO.output(ForwardPort, False)
                GPIO.output(ReversePort, False)
            else:
                GPIO.output(ForwardStarboard, True)
                GPIO.output(ReverseStarboard, False)
                GPIO.output(ForwardPort, False)
                GPIO.output(ReversePort, False)
        elif pygame.bRight == True:
            if pygame.bReverse == True:
                GPIO.output(ForwardStarboard, False)
                GPIO.output(ReverseStarboard, False)
                GPIO.output(ForwardPort, False)
                GPIO.output(ReversePort, True)
            else:
                GPIO.output(ForwardStarboard, False)
                GPIO.output(ReverseStarboard, False)
                GPIO.output(ForwardPort, True)
                GPIO.output(ReversePort, False)
        
    elif pygame.bForward == False and pygame.bReverse == False:
        if pygame.bLeft == True:
            GPIO.output(ForwardStarboard, True)
            GPIO.output(ReverseStarboard, False)
            GPIO.output(ForwardPort, False)
            GPIO.output(ReversePort, True)
        elif pygame.bRight == True:
            GPIO.output(ForwardStarboard, False)
            GPIO.output(ReverseStarboard, True)
            GPIO.output(ForwardPort, True)
            GPIO.output(ReversePort, False)
            
    if pygame.bLeft == False and pygame.bRight == False:
        if pygame.bForward == True and pygame.bReverse == False:
            GPIO.output(ForwardStarboard, True)
            GPIO.output(ReverseStarboard, False)
            GPIO.output(ForwardPort, True)
            GPIO.output(ReversePort, False)
        elif pygame.bReverse == True and pygame.bForward == False:
            GPIO.output(ForwardStarboard, False)
            GPIO.output(ReverseStarboard, True)
            GPIO.output(ForwardPort, False)
            GPIO.output(ReversePort, True)
        else:
            GPIO.output(ForwardStarboard, False)
            GPIO.output(ReverseStarboard, False)
            GPIO.output(ForwardPort, False)
            GPIO.output(ReversePort, False)
            
    if pygame.bCenteringDome == False:
        if pygame.bLookRight == True or pygame.bLookLeft == True:
            if pygame.bLookRight == True:
                GPIO.output(DomeRotateRight, True)
                GPIO.output(DomeRotateLeft, False)
            else:
                GPIO.output(DomeRotateRight, False)
                GPIO.output(DomeRotateLeft, True)
        elif pygame.bLookRight == False and pygame.bLookLeft == False:
            GPIO.output(DomeRotateLeft, False)
            GPIO.output(DomeRotateRight, False)

#################################################################

# These are server marcros, called from buttons on the HTML page or keyboard events.

@webiopi.macro
def goForward():
    if pygame.bForward == False:
        pygame.bForward = True
        stopReverse()

@webiopi.macro
def goReverse():
    if pygame.bReverse == False:
        pygame.bReverse = True
        stopForward()

@webiopi.macro   
def goLeft():
    if pygame.bLeft == False:
        pygame.bLeft = True
        stopRight()

@webiopi.macro    
def goRight():
    if pygame.bRight == False:
        pygame.bRight = True
        stopLeft()

@webiopi.macro    
def stopForward():
    if pygame.bForward == True:
        pygame.bForward = False

@webiopi.macro
def stopReverse():
    if pygame.bReverse == True:
        pygame.bReverse = False

@webiopi.macro    
def stopLeft():
    if pygame.bLeft == True:
        pygame.bLeft = False

@webiopi.macro    
def stopRight():
    if pygame.bRight == True:
        pygame.bRight = False

@webiopi.macro    
def halt():
    pygame.bForward = False
    pygame.bReverse = False
    pygame.bLeft = False
    pygame.bRight = False
    
@webiopi.macro
def lookLeft():
    if pygame.bLookLeft == False:
        pygame.bLookLeft = True
        stopLookRight()

@webiopi.macro
def lookRight():
    if pygame.bLookRight == False:
        pygame.bLookRight = True
        stopLookLeft()

@webiopi.macro
def stopLookLeft():
    pygame.bLookLeft = False

@webiopi.macro
def stopLookRight():
    pygame.bLookRight = False